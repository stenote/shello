﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SWAT
{
    [ComVisible(true)]
    public class Geolocation
    {
        [ComVisible(true)]
        public class Coordinates
        {
            public double latitude;
            public double longitude;
            public double speed;
            public double accuracy;
            public double altitude;
            public double altitudeAccuracy;
            public double heading;
        }

        [ComVisible(true)]
        public class Postition
        {
            public Coordinates coords;
            public UInt64 timestamp;
        }

        [ComVisible(true)]
        public class PositionError
        {
            public UInt16 code;
            public String message;
        }

        const UInt16 PERMISSION_DENIED = 1;
        const UInt16 POSITION_UNAVAILABLE = 2;
        const UInt16 TIMEOUT = 3;

        private Dictionary<long, GeoCoordinateWatcher> _watchers = new Dictionary<long, GeoCoordinateWatcher>();
        private long _watchId = 0;

        public void getCurrentPosition(dynamic successCallback, dynamic errorCallback = null, dynamic options = null)
        {
            Start(successCallback, errorCallback, options, false);
        }

        public long watchPosition(dynamic successCallback, dynamic errorCallback = null, dynamic options = null)
        {
            var watcher = Start(successCallback, errorCallback, options, true);
            var id = _watchId++;
            _watchers.Add(id, watcher);
            return id;
        }

        public void clearWatch(long watchId) {
            if (_watchers.ContainsKey(watchId)) {
                _watchers[watchId].Stop();
                _watchers.Remove(watchId);
            }
        }

        private GeoCoordinateWatcher Start(dynamic successCallback, dynamic errorCallback, dynamic options, bool watchMode) {
            var desiredAccuracy = GeoPositionAccuracy.Default;
            long timeout = -1;
            if (options != null) {
                try {
                    if (options.enableHighAccuracy.GetType() == typeof(bool) && options.enableHighAccuracy) {
                        desiredAccuracy = GeoPositionAccuracy.High;
                    }
                } catch (Exception) { }
                try {
                    timeout = options.timeout;
                } catch (Exception) { }
            }

            var watcher = new GeoCoordinateWatcher(desiredAccuracy);
            if (watcher.Permission == GeoPositionPermission.Denied) {
                if (errorCallback != null) {
                    try {
                        errorCallback(new PositionError() {
                            code = PERMISSION_DENIED,
                            message = "PERMISSION_DENIED",
                        });
                    } catch (Exception) { }
                }
                return null;
            }

            Timer timer = null;
            if (timeout > 0 && !watchMode) {
                timer = new Timer(timeout);
                timer.AutoReset = false;
                timer.Elapsed += (sender, e) => {
                    MainThread.Invoke(() => {
                        watcher.Stop();
                        timer.Stop();
                        if (errorCallback != null) {
                            try {
                                errorCallback(new PositionError() {
                                    code = TIMEOUT,
                                    message = "TIMEOUT",
                                });
                            } catch (Exception) { }
                        }
                    });
                };
                timer.Start();
            }

            bool handled = false;
            watcher.PositionChanged += (sender, e) => {
                if (!watchMode) {
                    if (handled) {
                        return;
                    }
                    handled = true;
                    watcher.Stop();
                    if (timer != null) {
                        timer.Stop();
                    }
                }

                var location = e.Position.Location;
                if (location.IsUnknown) {
                    if (errorCallback != null) {
                        try {
                            errorCallback(new PositionError() {
                                code = POSITION_UNAVAILABLE,
                                message = "POSITION_UNAVAILABLE",
                            });
                        } catch (Exception) {}
                    }
                    return;
                }

                if (successCallback != null) {
                    try {
                        successCallback(new Postition() {
                            coords = new Coordinates() {
                                latitude = location.Latitude,
                                longitude = location.Longitude,
                                altitude = location.Altitude,
                                accuracy = location.HorizontalAccuracy,
                                altitudeAccuracy = location.VerticalAccuracy,
                                heading = location.Course,
                                speed = location.Speed
                            },
                            timestamp = (UInt64)e.Position.Timestamp.UtcDateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds,
                        });
                    } catch (Exception) { }
                }
            };
            watcher.Start();
            return watcher;
        }
    }
}
