﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Device.Location;

namespace SWAT
{
    public partial class Form1 : Form
    {
        [DataContract]
        class Config
        {
            [DataMember]
            public int windowWidth { get; set; }
            [DataMember]
            public int windowHeight { get; set; }
            [DataMember]
            public string windowTitle { get; set; }
            [DataMember]
            public string windowURI { get; set; }
        }

        public string _windowURI = "http://127.0.0.1:1080/";
        
        public Form1()
        {
            InitializeComponent();

            MainThread.Init(this);

            webBrowser1.ScrollBarsEnabled = false;
            webBrowser1.ObjectForScripting = new WebInterface();

            var path = System.AppDomain.CurrentDomain.BaseDirectory;

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Config));
            var minSize = new Size();
            try {
                using (var stream = new FileStream(path + "config.json", FileMode.Open)) {
                    var config = serializer.ReadObject(stream) as Config;
                    if (config.windowWidth > 0) {
                        minSize.Width = config.windowWidth;
                    }
                    if (config.windowHeight > 0) {
                        minSize.Height = config.windowHeight;
                    }
                    if (!string.IsNullOrWhiteSpace(config.windowTitle)) {
                        this.Text = config.windowTitle;
                    }
                    if (!string.IsNullOrWhiteSpace(config.windowURI)) {
                        this._windowURI = config.windowURI;
                    }
                }
            } catch (Exception) {

            }
            this.MinimumSize = minSize;

            if (Properties.Settings.Default.FirstRun) {
                this.CenterToScreen();
                if (minSize.Width == 0) {
                    this.Width = 800;
                }
                if (minSize.Height == 0) {
                    this.Height = 600;
                }
                Properties.Settings.Default.FirstRun = true;
            } else {
                if (Properties.Settings.Default.Maximised) {
                    WindowState = FormWindowState.Maximized;
                    Location = Properties.Settings.Default.Location;
                    Size = Properties.Settings.Default.Size;
                } else {
                    Location = Properties.Settings.Default.Location;
                    Size = Properties.Settings.Default.Size;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate(_windowURI);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (WindowState == FormWindowState.Normal) {
                Properties.Settings.Default.Location = Location;
                Properties.Settings.Default.Size = Size;
            } else {
                Properties.Settings.Default.Location = RestoreBounds.Location;
                Properties.Settings.Default.Size = RestoreBounds.Size;
            }
            Properties.Settings.Default.Maximised = WindowState == FormWindowState.Maximized;
            if (Properties.Settings.Default.FirstRun) {
                Properties.Settings.Default.FirstRun = false;
            }
            Properties.Settings.Default.Save();

            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }
}
