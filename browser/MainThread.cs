﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWAT
{
    class MainThread
    {
        private static Control _control;
        public static void Init(Control control) {
            _control = control;
        }

        public static void Invoke(Action action)
        {
            _control.Invoke(action);
        }
    }
}
